#! /usr/bin/env bash


echo "check whether current user is already in the sudoers"
if [[ ! " $(groups) " =~ ' sudo ' ]]; then
    echo "FAILURE: add ${USER} to the group sudo"
    echo "  - become root: su -"
    echo "  - adduser bioinfo sudo"
    echo "  - exit root: exit"
    echo "  - newgrp sudo (or logout and login again)"
    echo "  - relaunch this script"
    echo ""
    exit -1
else
    echo "${USER} is in group sudo"
    echo "DONE"
    echo ""
fi


echo "check for packages to update"
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get clean
echo "DONE"
echo ""


echo "install git"
sudo apt-get install git
echo "DONE"
echo ""


echo "autoupdate from git repository"
if [ ! -d ".git" ]
then
    if [ -d linuxConfig ]
    then
        cd linuxConfig
        git pull
    else
        git clone https://gitlab.com/odameron/linuxConfig.git
        cd linuxConfig
    fi
    cd ..
    rm installLocalConfiguration.bash
    ln -s linuxConfig/installLocalConfiguration.bash installLocalConfiguration.bash
    if [ -e .bashrc ]
    then
        rm .bashrc
    fi
    ln -s linuxConfig/.bashrc .bashrc
fi
echo "DONE"
echo ""


tstamp=`date +%Y%m%d-%H%M%S`
echo "replace .bashrc"
cp ~/.bashrc ~/.bashrc-${tstamp}
cp .bashrc ~/.bashrc
source ~/.bashrc
echo "DONE\n"
#echo "SKIPPED"
echo ""


echo "install java"
# info from https://wiki.debian.org/Java
#sudo apt-get install default-jdk
#echo "DONE"
echo "SKIPPED"
echo ""


echo "install basic packages"
sudo apt-get install colordiff
sudo apt-get install htop
sudo apt-get install jupyter-nbextension-jupyter-js-widgets jupyter-notebook
sudo apt-get install openssh-client openssh-server
sudo apt-get install snakemake
sudo apt-get install thonny
echo "DONE"
echo ""


echo "install python3"
sudo apt-get install python3
echo "DONE"
echo ""


echo "install julia"
sudo apt-get install julia
echo "DONE"
echo ""


echo "install discord"
echo "  download discord (take the deb file) from https://discord.com/download"
echo "  cd ~/Downloads"
echo "  sudo apt-get install libc++1"
echo "  sudo dpkg -i discord*.deb"
echo "  rm discord*.deb"
echo "  cd -"
echo "  discord"
echo "  #join bioinfo-fr server: visit https://bioinfo-fr.net/discord"
echo "  #join e-bigo server"
#firefox --new-tab https://discord.com/download
#firefox --new-tab https://bioinfo-fr.net/discord
echo "SKIPPED"
echo ""


echo "clean installation"
sudo apt-get clean
echo "DONE"
echo ""

