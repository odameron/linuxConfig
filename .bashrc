# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
#export HISTCONTROL=ignoredups

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    #alias dir='ls --color=auto --format=vertical'
    #alias vdir='ls --color=auto --format=long'
fi

# some more ls aliases
alias ll='ls -l --color=auto'
alias lal='ls -al --color=auto'
#alias la='ls -A'
#alias l='ls -CF'


# Other aliases
alias retract='eject -t'
alias lockTerminal='cmatrix -s'
alias stopwatch='java -jar /usr/local/stopwatch/Stopwatch.jar'
alias diff='colordiff'
alias musb='mount /mnt/usb'
alias umusb='umount /mnt/usb'
function cpusb { cp "$1" /mnt/usb ;}
alias musbr='mount /mnt/usbRemote'
alias umusbr='umount /mnt/usbRemote'
function cpusbr { cp "$1" /mnt/usbRemote ;}
alias lusb='ll /mnt/usb'
alias lusbr='ll /mnt/usbRemote'
alias mountPhone='jmtpfs /mnt/phone'
alias umountPhone='fusermount -u /mnt/phone'
alias dpkgrep='dpkg -l | grep'
alias javar='java -jar'
alias aptfix='sudo apt --fix-broken install'
alias cdfuseki='cd /usr/local/semanticWeb/fuseki'
function cdl { cd "$1"; ll;}

alias conjugue='french-conjugator'
alias conjuguePresent='french-conjugator --mode=indicative --tense=present'
#alias tailLog='tail -f /var/log/messages'
alias tailLog='tail -f /var/log/syslog'

alias gringo='/usr/local/clasp/gringo'
alias clasp='/usr/local/clasp/clasp'
alias clingo='/usr/local/clingo/clingo'

alias org='emacs ${HOME}/config/org/index.org'
alias rdfXML2N3='java -cp /usr/local/jena/lib/jena.jar:/usr/local/jena/lib/xercesImpl.jar:/usr/local/jena/lib/iri.jar:/usr/local/jena/lib/icu4j_3_4.jar com.hp.hpl.jena.rdf.arp.NTriple'
#alias skype='/usr/local/skype/skype'
alias loffice='libreoffice'
alias vpnui='/opt/cisco/anyconnect/bin/vpnui'
alias startvirtuoso='sudo /etc/init.d/virtuoso-opensource-6.1 start'
alias stopvirtuoso='sudo /etc/init.d/virtuoso-opensource-6.1 stop'
alias statusvirtuoso='sudo /etc/init.d/virtuoso-opensource-6.1 status'
alias pellet='/usr/local/semanticWeb/pellet/pellet.sh'
#export JENAROOT=/usr/local/semanticWeb/jena
#alias arq='${JENAROOT}/bin/arq'
alias fuseki-go='/usr/local/semanticWeb/fuseki/fuseki-server --update --file=/home/olivier/ontology/geneOntology/go-termdb-20121005-11h02m04s.owl /go'
alias gephi='/usr/local/gephi/bin/gephi'
alias mouseSlow='xset m 1'
#alias pdfstudio='/usr/local/pdfstudio/pdfstudio8/pdfstudio8'
#alias pubmed='firefox --new-tab https://www.ncbi.nlm.nih.gov/pubmed/'



# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" -a -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color)
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    ;;
*)
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    ;;
esac

# Comment in the above and uncomment this below for a color prompt
#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
# Cyan:
#PS1="\[\e[36;1m\]\u\[\e[0m\]@\h \[\e[37;1m\]\w\[\e[0m\]> "
# Red:
#PS1="\[\e[31;1m\]\u\[\e[0m\]@\h \[\e[37;1m\]\w\[\e[0m\]> "
# Yellow
#PS1="\[\e[33;1m\]\u\[\e[0m\]@\h \[\e[37;1m\]\w\[\e[0m\]> "
# Magenta:
#PS1="\[\e[35;1m\]\u\[\e[0m\]@\h \[\e[37;1m\]\w\[\e[0m\]> "
# Green
#PS1="\[\e[32;1m\]\u\[\e[0m\]@\h \[\e[37;1m\]\w\[\e[0m\]> "
PS1="\[\e[32;1m\]\u\[\e[0m\]@\[\e[36;1m\]\h \[\e[37;1m\]\w\[\e[0m\]> "

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
    ;;
*)
    ;;
esac

# Define your own aliases here ...
#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi
alias kile='kile 2>/dev/null'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#fi

#export PATH=${PATH}:~/bin
export PATH=${PATH}:/usr/local/semanticWeb/robot

export CATALINA_HOME=/usr/local/tomcat
export AXIS_HOME=${CATALINA_HOME}/webapps/axis
export AXIS_LIB=${AXIS_HOME}/WEB-INF/lib
#export JENA_HOME=/usr/local/jena
#export JENA_LIB=${JENA_HOME}/lib
export JENA_HOME=/usr/local/semanticWeb/jena
export PELLET_HOME=/usr/local/semanticWeb/pellet
export FUSEKI_HOME=/usr/local/semanticWeb/fuseki
export TARQL_HOME=/usr/local/semanticWeb/tarql

export PROTEGE_HOME=/usr/local/semanticWeb/protege
alias protege='/usr/local/semanticWeb/protege/Protege'
export PROTEGE4_HOME=/usr/local/semanticWeb/protege4
alias protege4='/usr/local/semanticWeb/protege4/Protege'

#alias eclipse='/usr/local/eclipse/eclipse'
alias welkin='/usr/local/welkin/welkin.sh'
alias camelis='/usr/local/camelis'
alias cytoscape='/usr/local/cytoscape/cytoscape.sh'

# get rid of Joplin's annoying notification on startup
#export APPIMAGE_SILENT_INSTALL=false

# for okular
export TZ="Europe/Paris"
#export LC_TIME=fr_FR

function renameTerm() {
  if [ $# -ne 1 ]; then 
     echo "Usage : $0 <title>" 
     #exit 1
    return 1
fi


case $TERM in
aterm*|cygwin|dtterm|emu|eterm*|gnome-terminal|hanterm|iris-ansi|konsole|kvt|mterm|mxterm|nxterm|kterm|rxvt|sun-cmd|wterm|xgterm|xiterm|*xterm)
        PROMPT_COMMAND='echo -ne "\033]0;'${1}'\007"'
;;
linux*)
        # Do nothing
;;
esac

}


function bibsearch-author() {
if [ $# -ne 1 ]; then
     echo "Usage : $0 <author name>" 
     #exit 1
    return 1
fi

for currentFile in `ls /home/olivier/biblio/bibtex/*.bib`;  do
	bib2bib -q -c 'author : "'$1'"' ${currentFile}
done

}

# mouse sensibility
xset m 1




# simple math operation
# ex: calc 3*3
function calc () {
    bc -l <<< "$@"
}

function age () {
    dateutils.ddiff $1 today -f '%y %m %d'
}


## KLUDGE FOR LIBREOFFICE CALC
#export SAL_DISABLE_SYNCHRONOUS_PRINTER_DETECTION="true"
